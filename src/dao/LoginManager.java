package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entitie.Comic;
import entitie.Genre;
import entitie.User;
import entitie.Character;

public class LoginManager {

	public User getUser(String login, String password) {
		Connection con = DaoFactory.getInstance().getConection();
		PreparedStatement statement;
		ResultSet rs;
		User user = null;
		try {
			statement = con.prepareStatement("SELECT * FROM comics.user usr WHERE usr.login = ? AND usr.password = ? ");
			statement.setString(1, login);
			statement.setString(2, password);
			rs = statement.executeQuery();
			while (rs.next()) {
				user = new User();
				user.setAge(rs.getInt("age"));
				user.setIduser(rs.getInt("id"));
				user.setLastNames(rs.getString("lastname"));
				user.setLogin(rs.getString("login"));
				user.setName(rs.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (Exception e2) {
				return null;
			}
		}
		// statement.
		return user;
	}

	public List<Comic> getAllComics() {
		Connection con = DaoFactory.getInstance().getConection();
		PreparedStatement statement;
		ResultSet rs;
		List<Comic> results = new ArrayList<Comic>();
		Comic comic;
		try {
			statement = con.prepareStatement("SELECT * FROM comic");
			rs = statement.executeQuery();
			while (rs.next()) {
				comic = new Comic();
				comic.setAuthor(rs.getString("author"));
				comic.setDatePublished(rs.getString("date_published"));
				comic.setEdition(rs.getNString("edition"));
				comic.setIdcomic(rs.getInt("id"));
				comic.setTitle(rs.getString("title"));
				comic.setImage(rs.getString("image"));
				results.add(comic);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			try {
				con.close();
			} catch (Exception e2) {
				return null;
			}
		}
		return results;
	}

	public List<Genre> getAllGenres() {
		Connection con = DaoFactory.getInstance().getConection();
		PreparedStatement statement;
		ResultSet rs;
		List<Genre> results = new ArrayList<Genre>();
		Genre genre;
		try {
			statement = con.prepareStatement("SELECT * FROM genre");
			rs = statement.executeQuery();
			while (rs.next()) {
				genre = new Genre();
				genre.setAge(rs.getInt("age"));
				genre.setName(rs.getString("name"));
				genre.setId(rs.getInt("id"));
				results.add(genre);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			try {
				con.close();
			} catch (Exception e2) {
				return null;
			}
		}
		return results;
	}

	public List<Character> getAllCharacters() {
		Connection con = DaoFactory.getInstance().getConection();
		PreparedStatement statement;
		ResultSet rs;
		List<Character> results = new ArrayList<Character>();
		Character character;
		try {
			statement = con.prepareStatement("SELECT * FROM comics.`character`");
			rs = statement.executeQuery();
			while (rs.next()) {
				character = new Character();
				character.setIdentity(rs.getString("identity"));
				character.setName(rs.getString("name"));
				character.setId(rs.getInt("id"));
				results.add(character);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			try {
				con.close();
			} catch (Exception e2) {
				return null;
			}
		}
		return results;
	}

	public List<Comic> getByIdCharacter(int idCharacter) {
		Connection con = DaoFactory.getInstance().getConection();
		PreparedStatement statement;
		ResultSet rs;
		List<Comic> results = new ArrayList<Comic>();
		Character character;
		Comic comic;
		try {
			statement = con.prepareStatement(
					"SELECT c.* FROM comic c , comic_character cc  WHERE c.id = cc.id_comic AND cc.id_character = ? ");
			statement.setInt(1, idCharacter);
			rs = statement.executeQuery();
			while (rs.next()) {
				comic = new Comic();
				comic.setAuthor(rs.getString("author"));
				comic.setDatePublished(rs.getString("date_published"));
				comic.setEdition(rs.getString("edition"));
				comic.setIdcomic(rs.getInt("id"));
				comic.setTitle(rs.getString("title"));
				comic.setImage(rs.getString("image"));
				results.add(comic);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (Exception e2) {
				return null;
			}
		}
		return results;
	}

	public List<Comic> getByIdGenre(int idGenre) {
		Connection con = DaoFactory.getInstance().getConection();
		PreparedStatement statement;
		ResultSet rs;
		List<Comic> results = new ArrayList<Comic>();
		Character character;
		Comic comic;
		try {
			statement = con.prepareStatement(
					"SELECT c.* FROM comic c , comic_genre cg  WHERE c.id = cg.id_comic AND cg.id_genre = ? ");
			statement.setInt(1, idGenre);
			rs = statement.executeQuery();
			while (rs.next()) {
				comic = new Comic();
				comic.setAuthor(rs.getString("author"));
				comic.setDatePublished(rs.getString("date_published"));
				comic.setEdition(rs.getString("edition"));
				comic.setIdcomic(rs.getInt("id"));
				comic.setTitle(rs.getString("title"));
				comic.setImage(rs.getString("image"));
				results.add(comic);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (Exception e2) {
				return null;
			}
		}
		return results;
	}

	public List<Comic> getComicById(int idComic) {
		Connection con = DaoFactory.getInstance().getConection();
		PreparedStatement statement;
		ResultSet rs;
		Character character;
		Comic comic = null;
		List<Comic> comics = new ArrayList<Comic>();
		try {
			statement = con.prepareStatement("SELECT c.* FROM comic c  WHERE c.id =  ? ");
			statement.setInt(1, idComic);
			rs = statement.executeQuery();
			while (rs.next()) {
				comic = new Comic();
				comic.setAuthor(rs.getString("author"));
				comic.setDatePublished(rs.getString("date_published"));
				comic.setEdition(rs.getString("edition"));
				comic.setIdcomic(rs.getInt("id"));
				comic.setTitle(rs.getString("title"));
				comic.setImage(rs.getString("image"));
				comics.add(comic);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (Exception e2) {
				return null;
			}
		}
		return comics;
	}

}
