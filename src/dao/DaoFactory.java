package dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DaoFactory {

	private static DaoFactory Instance = new DaoFactory();

	public static DaoFactory getInstance() {
		return Instance;
	}

	public Connection getConection() {
		Context ctx;
		try {
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:/Comics");
			return ds.getConnection();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return null;
		}

	}

}
