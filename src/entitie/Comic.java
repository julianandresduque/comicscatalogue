package entitie;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the comic database table.
 * 
 */

public class Comic implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idcomic;
	private String author;
	private String datePublished;
	private String edition;
	private String title;
	private String image;
	
	public Comic() {
	}

	public int getIdcomic() {
		return this.idcomic;
	}

	public void setIdcomic(int idcomic) {
		this.idcomic = idcomic;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDatePublished() {
		return this.datePublished;
	}

	public void setDatePublished(String datePublished) {
		this.datePublished = datePublished;
	}

	public String getEdition() {
		return this.edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	

}