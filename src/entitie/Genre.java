package entitie;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the genre database table.
 * 
 */

public class Genre implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private int age;
	private String name;
	private Comic comic;

	public Genre() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	//bi-directional many-to-one association to Comic
	
	public Comic getComic() {
		return this.comic;
	}

	public void setComic(Comic comic) {
		this.comic = comic;
	}

}