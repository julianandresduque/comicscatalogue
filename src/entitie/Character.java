package entitie;

import java.io.Serializable;

/**
 * The persistent class for the character database table.
 * 
 */

public class Character implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;

	private String identity;

	private String name;

	private Comic comic;

	public Character() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdentity() {
		return this.identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// bi-directional many-to-one association to Comic

	public Comic getComic() {
		return this.comic;
	}

	public void setComic(Comic comic) {
		this.comic = comic;
	}

}