package socket;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import entitie.Character;
import entitie.Comic;
import entitie.Genre;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.spi.JsonProvider;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import dao.LoginManager;

@ServerEndpoint("/dataServer")
public class DataServer {

	@OnOpen
	public void onOpen(Session session) {
		System.out.println("WebSocket opened: " + session.getId());
	}

	@OnClose
	public void onClose(CloseReason reason, Session session) {
		System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
	}

	@OnMessage
	public void getData(Session session, String data) throws IOException {
		JsonReader reader = Json.createReader(new StringReader(data));
		JsonObject jsonMessage = reader.readObject();
		LoginManager loginManager = new LoginManager();
		if (jsonMessage.getString("request").equals("character")) {
			session.getBasicRemote().sendText(buildJsonResponseCharacter(loginManager.getAllCharacters()).toString());
		}
		if (jsonMessage.getString("request").equals("comic")) {
			session.getBasicRemote().sendText(buildJsonResponseComics(loginManager.getAllComics()).toString());
		}
		if (jsonMessage.getString("request").equals("genre")) {
			session.getBasicRemote().sendText(buildJsonResponseGenres(loginManager.getAllGenres()).toString());
		}
	}

	private JsonArray buildJsonResponseCharacter(List<Character> characters) {
		JsonArrayBuilder builder = JsonProvider.provider().createArrayBuilder();
		for (Character character : characters) {
			builder.add(JsonProvider.provider().createObjectBuilder().add("id", character.getId())
					.add("name", character.getName()).add("identity", character.getIdentity()));
		}
		return builder.build();
	}

	private JsonArray buildJsonResponseComics(List<Comic> comics) {
		JsonArrayBuilder builder = JsonProvider.provider().createArrayBuilder();
		for (Comic comic : comics) {
			builder.add(JsonProvider.provider().createObjectBuilder().add("id", comic.getIdcomic())
					.add("edition", comic.getEdition()).add("date_published", comic.getDatePublished())
					.add("author", comic.getAuthor()));
		}
		return builder.build();
	}

	private JsonArray buildJsonResponseGenres(List<Genre> genres) {
		JsonArrayBuilder builder = JsonProvider.provider().createArrayBuilder();
		for (Genre genre : genres) {
			builder.add(JsonProvider.provider().createObjectBuilder().add("id", genre.getId())
					.add("name", genre.getName()).add("age", genre.getAge()));
		}
		return builder.build();
	}

}
