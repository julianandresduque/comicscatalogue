package socket;

import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.spi.JsonProvider;
import javax.naming.NamingException;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import dao.LoginManager;
import entitie.Character;
import entitie.Comic;
import entitie.Genre;
import entitie.User;

@ServerEndpoint("/loginServer")
public class LoginServer {

	@OnOpen
	public void onOpen(Session session) {
		System.out.println("WebSocket opened: " + session.getId());
	}

	@OnClose
	public void onClose(CloseReason reason, Session session) {
		System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());

	}

	@OnMessage
	public void login(Session session, String login) throws IOException, NamingException, SQLException {
		JsonReader reader = Json.createReader(new StringReader(login));
		JsonObject jsonMessage = reader.readObject();
		LoginManager loginManager = new LoginManager();
		if (jsonMessage.getString("action").equals("login")) {
			if (jsonMessage.getString("login") == null || jsonMessage.getString("login").equals("")
					|| jsonMessage.getString("password") == null || jsonMessage.getString("password").equals("")) {
				JsonObject response = Json.createObjectBuilder().add("response", "login")
						.add("user", createFailedUserUserToSend("credentialsNotTyped")).build();
				System.out.println(response.toString());
				session.getBasicRemote().sendText(response.toString());
			} else {
				User user = loginManager.getUser(jsonMessage.getString("login"), jsonMessage.getString("password"));
				JsonObject response = user != null
						? Json.createObjectBuilder().add("response", "login").add("user", createUserToSend(user))
								.add("comics", buildJsonResponseComics(loginManager.getAllComics()))
								.add("characters", buildJsonResponseCharacter(loginManager.getAllCharacters()))
								.add("genres", buildJsonResponseGenres(loginManager.getAllGenres())).build()
						: Json.createObjectBuilder().add("response", "login")
								.add("user", createFailedUserUserToSend("loginFailed")).build();
				System.out.println(response.toString());
				session.getBasicRemote().sendText(response.toString());
			}
		} else if (jsonMessage.getString("action").equals("getComicsByCharacter")) {
			JsonObject response = Json.createObjectBuilder().add("response", "getComicsByCharacter")
					.add("comics", buildJsonResponseComics(loginManager.getByIdCharacter(jsonMessage.getInt("id"))))
					.build();
			System.out.println(response.toString());
			session.getBasicRemote().sendText(response.toString());
		} else if (jsonMessage.getString("action").equals("getComicsByGenre")) {
			JsonObject response = Json.createObjectBuilder().add("response", "getComicsByGenre")
					.add("comics", buildJsonResponseComics(loginManager.getByIdGenre(jsonMessage.getInt("id"))))
					.build();
			System.out.println(response.toString());
			session.getBasicRemote().sendText(response.toString());
		} else if (jsonMessage.getString("action").equals("getComicsById")) {
			JsonObject response = Json.createObjectBuilder().add("response", "getComicsById")
					.add("comics", buildJsonResponseComics(loginManager.getComicById(jsonMessage.getInt("id")))).build();
			System.out.println(response.toString());
			session.getBasicRemote().sendText(response.toString());
		}
	}

	private JsonObject createUserToSend(User user) {
		return JsonProvider.provider().createObjectBuilder().add("status", "loginok").add("login", user.getLogin())
				.add("name", user.getName()).add("lastname", user.getLastNames()).add("age", user.getAge()).build();
	}

	private JsonObject createFailedUserUserToSend(String message) {
		return JsonProvider.provider().createObjectBuilder().add("status", message).build();
	}

	private JsonObject createFailResponse() {
		return JsonProvider.provider().createObjectBuilder().add("status", "loginfail").build();
	}

	private JsonArray buildJsonResponseCharacter(List<Character> characters) {
		JsonArrayBuilder builder = JsonProvider.provider().createArrayBuilder();
		for (Character character : characters) {
			builder.add(JsonProvider.provider().createObjectBuilder().add("id", character.getId())
					.add("name", character.getName()).add("identity", character.getIdentity()));
		}
		return builder.build();
	}

	private JsonArray buildJsonResponseComics(List<Comic> comics) {
		JsonArrayBuilder builder = JsonProvider.provider().createArrayBuilder();
		for (Comic comic : comics) {
			builder.add(JsonProvider.provider().createObjectBuilder().add("id", comic.getIdcomic())
					.add("edition", comic.getEdition()).add("date_published", comic.getDatePublished())
					.add("author", comic.getAuthor()).add("tittle", comic.getTitle()).add("image", comic.getImage()));
		}
		return builder.build();
	}

	private JsonArray buildJsonResponseGenres(List<Genre> genres) {
		JsonArrayBuilder builder = JsonProvider.provider().createArrayBuilder();
		for (Genre genre : genres) {
			builder.add(JsonProvider.provider().createObjectBuilder().add("id", genre.getId())
					.add("name", genre.getName()).add("age", genre.getAge()).add("id", genre.getId()));
		}
		return builder.build();
	}

	

}
