$(document).ready(function() {
	$("#menubar").css("display", "none");
	$("#fieldsblank").css("display", "none");
	$("#loginerror").css("display", "none");
	$("#detailsUserPopUp").css("display", "none");
	$("#log_anchor").css("display", "none");
})

function cleaErrorMessages() {
	$("#fieldsblank").css("display", "none");
	$("#loginerror").css("display", "none");
}

var socket = new WebSocket("ws://localhost:8080/ComicsCatalogue/loginServer");
socket.onmessage = onMessage;

function onMessage(event) {
	cleaErrorMessages();
	var response = JSON.parse(event.data);
	alert(response.response);
	if (response.response == "login") {
		if (response.user.status == "loginok") {
			closePopup('loginsection');
			localStorage.setItem("userLogin", response.user.login);
			localStorage.setItem("userName", response.user.name);
			localStorage.setItem("userLastName", response.user.lastname);
			populateMenus(response);
			showElement('menubar');
			showElement('log_anchor');
		} else if (response.user.status == "credentialsNotTyped") {
			showElement('fieldsblank');
		} else if (response.user.status == "loginFailed") {
			showElement('loginerror');
		}
	} else if (response.response == "getComicsByCharacter") {
		populateComics(response.comics);
	} else if (response.response == "getComicsByGenre") {
		populateComics(response.comics);
	} else if (response.response == "getComicsById") {
		populateComics(response.comics);
	}
}

function populateMenus(response) {
	$.each(response.genres,
			function(index, value) {
				$("#genres").append(
						'<li onclick="getComicsByGenre(' + value.id
								+ ')">  <a href="#header">' + value.name
								+ '</a></li>');

			});
	$.each(response.characters,
			function(index, value) {
				$("#characters").append(
						'<li onclick="getComicsByCharacter(' + value.id
								+ ')">  <a href="#header">' + value.name
								+ '</a></li>');

			});
	$.each(response.comics, function(index, value) {
		$("#comics").append(
				'<li onclick="getComicById(' + value.id
						+ ')">  <a href="#header">' + value.tittle
						+ '</a></li>');

	});

}

function login(log, pass) {
	var user = {
		action : "login",
		login : log,
		password : pass
	};
	socket.send(JSON.stringify(user));
}

function getComicsByCharacter(idCharacter) {
	var request = {
		action : "getComicsByCharacter",
		id : idCharacter
	};
	socket.send(JSON.stringify(request));
}

function getComicsByGenre(idGenre) {
	var request = {
		action : "getComicsByGenre",
		id : idGenre
	};
	socket.send(JSON.stringify(request));
}

function getComicById(idComic) {
	var request = {
		action : "getComicsById",
		id : idComic
	};
	socket.send(JSON.stringify(request));
}

function closePopup(popup) {
	document.getElementById(popup).style.display = 'none';
	$(".wrap").css('display', 'none');
}

function showElement(popup) {
	$("#" + popup).css("display", '');
}

function populateComics(comics) {
	var parent = $('#latest');
	alert(comics.length);
	parent.empty();
	for (i = 0; i < comics.length; i++) {
		alert('iteration')
		if (i == (comics.length - 1)) {
			a = '<article class="one_quarter lastbox"><figure onclick="showDetails('
					+ comics[i].id
					+ ')"><img src="images/'
					+ comics[i].image
					+ '" width="215" height="315" alt=""><figcaption>'
					+ comics[i].tittle + '</figcaption> </figure> </article>';
			parent.append(a);
		} else {
			a = '<article class="one_quarter"><figure onclick="showDetails('
					+ comics[i].id + ')"><img src="images/' + comics[i].image
					+ '" width="215" height="315" alt=""><figcaption>'
					+ comics[i].tittle + '</figcaption> </figure></article>';
			parent.append(a);
		}
	}
}

function showPopup(popup) {
	document.getElementById(popup).style.display = 'block';
	$("#" + popup + "").fadeIn(500);
	$(".cover").fadeTo(500, 0.5).css('display', 'block');
}



function populateUserData() {
	alert('enter populateUserData');
	$('#usrLogin').text(localStorage.getItem("userLogin"));
	$('#usrName').text(localStorage.getItem("userName"));
	$('#usrLastName').text(localStorage.getItem("userLastName"));
	showPopup('detailsUserPopUp');
}
